import 'package:url_launcher/url_launcher.dart';

class UrlHelper {
  static void launchURL(String url, bool inApp) async {
    if (await canLaunch(url)) {
      if (inApp) {
        await launch(
          url,
          forceSafariVC: true,
          forceWebView: true,
          enableJavaScript: true,
        );
      } else {
        await launch(url);
      }
    } else {
      throw 'Could not launch $url';
    }
  }
}
