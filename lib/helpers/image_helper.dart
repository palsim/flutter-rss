class ImageHelper {
  static bool isImage(String? type) {
    if (type == null) return false;
    return (type == "image/jpg" || type == "image/jpeg" || type == "image/png");
  }
}
