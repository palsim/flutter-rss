import 'package:flutter/material.dart';
import 'package:webfeed/domain/rss_feed.dart';
import 'package:intl/intl.dart';
import '../screens/item_screen.dart';

class Item extends StatelessWidget {
  final String id;
  final String title;
  final DateTime pubDate;
  final RssFeed feed;

  const Item({
    required this.id,
    required this.title,
    required this.pubDate,
    required this.feed,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        foregroundImage:
            feed.image != null ? NetworkImage(feed.image!.url as String) : null,
      ),
      title: Text(
        title,
      ),
      subtitle: Text(DateFormat.yMMMd().add_jm().format(pubDate)),
      onTap: () {
        Navigator.of(context).pushNamed(ItemScreen.routeName, arguments: id);
      },
    );
  }
}
