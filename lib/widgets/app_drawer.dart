import 'package:flutter/material.dart';
import '../helpers/url_helper.dart';

class AppDrawer extends StatelessWidget {
  final Set<Map<String, String>> rssFeedsMap;
  final int selectedFeed;
  final Function changeFeed;

  const AppDrawer(
      {required this.rssFeedsMap,
      required this.selectedFeed,
      required this.changeFeed,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Widget> elements = [];
    elements.add(AppBar(
      title: const Text("Flutter RSS Feeds"),
      automaticallyImplyLeading: false,
    ));

    for (var i = 0; i < rssFeedsMap.length; ++i) {
      elements.add(ListTile(
        leading: const Icon(Icons.rss_feed),
        title: Text(
          rssFeedsMap.elementAt(i).values.elementAt(0),
        ),
        tileColor:
            selectedFeed == i ? Theme.of(context).colorScheme.background : null,
        onTap: () {
          changeFeed(i);
          Navigator.of(context).pop();
        },
      ));
    }

    elements.add(const Divider());
    elements.add(ListTile(
      leading: const Icon(Icons.link),
      title: const Text(
        "Google",
      ),
      onTap: () {
        UrlHelper.launchURL("https://google.com", true);
      },
    ));

    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: elements,
        ),
      ),
    );
  }
}
