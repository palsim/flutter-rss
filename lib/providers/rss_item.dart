import 'package:flutter/material.dart';
import 'package:webfeed/domain/rss_enclosure.dart';

class RSSItem with ChangeNotifier {
  final String id;
  final String title;
  final String link;
  final DateTime pubDate;
  final String description;
  final RssEnclosure? enclosure;

  RSSItem(
    this.id,
    this.title,
    this.link,
    this.pubDate,
    this.description,
    this.enclosure,
  );
}
