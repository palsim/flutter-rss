import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:webfeed/webfeed.dart';
import '../providers/rss_item.dart';
//import 'package:http/http.dart' as http;

class RSSFeed with ChangeNotifier {
  RssFeed _feed = RssFeed();
  final List<RSSItem> _items = [];

  List<RSSItem> get items {
    return [..._items];
  }

  RssFeed get feed {
    return _feed;
  }

  RSSItem findById(String id) {
    return _items.firstWhere((prod) => prod.id == id);
  }

  Future<void> fetchAndSetRSS(String url) async {
    try {
      final client = IOClient(HttpClient()
        ..badCertificateCallback =
            ((X509Certificate cert, String host, int port) => true));
      final response = await client.get(Uri.parse(url));

      // final uri = Uri.parse(url);
      // final response = await http.get(uri);

      _feed = RssFeed.parse(response.body);

      _items.clear();
      for (var element in _feed.items!) {
        _items.add(RSSItem(
          element.guid!,
          element.title!,
          element.link!,
          element.pubDate!,
          element.description!,
          element.enclosure,
        ));
      }
      notifyListeners();
    } catch (error) {
      print(error);
      rethrow;
    }
  }
}
