import 'dart:io';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/rss_feed.dart';
import './screens/item_screen.dart';
import './screens/rss_screen.dart';

void main() {
  //fixes NetworkImage error
  //https://stackoverflow.com/questions/54285172/how-to-solve-flutter-certificate-verify-failed-error-while-performing-a-post-req
  HttpOverrides.global = MyHttpOverrides();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => RSSFeed(),
        ),
      ],
      child: MaterialApp(
        home: const RssScreen(),
        theme: ThemeData.light(),
        darkTheme: ThemeData.dark(),
        themeMode: ThemeMode.system,
        routes: {
          ItemScreen.routeName: (ctx) => const ItemScreen(),
        },
      ),
    );
  }
}

//fixes NetworkImage error
//https://stackoverflow.com/questions/54285172/how-to-solve-flutter-certificate-verify-failed-error-while-performing-a-post-req
class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
