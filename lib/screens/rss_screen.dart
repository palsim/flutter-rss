import 'package:flutter/material.dart';
import '../widgets/app_drawer.dart';
import './items_screen.dart';

class RssScreen extends StatefulWidget {
  const RssScreen({Key? key}) : super(key: key);

  @override
  _RssScreenState createState() => _RssScreenState();
}

class _RssScreenState extends State<RssScreen> {
  int selectedFeed = 0;

  Set<Map<String, String>> rssFeedsMap = {
    {
      "title": "New York Times",
      "url": "https://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml"
    },
    //{"title": "CNN", "url": "http://rss.cnn.com/rss/cnn_topstories.rss"},
    //{"title": "Observador", "url": "https://observador.pt/feed/"},
    {
      "title": "Updates from Luke Smith",
      "url": "https://lukesmith.xyz/rss.xml"
    },
    {"title": "Peertube", "url": "https://lukesmith.xyz/peertube"},
    {
      "title": "Luke Smith on Odysee",
      "url": "https://odysee.com/\$/rss/@Luke/70"
    },
    {"title": "Not Related", "url": "https://notrelated.libsyn.com/rss"},
  };

  void changeFeed(idx) {
    setState(() {
      selectedFeed = idx;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(rssFeedsMap.elementAt(selectedFeed).values.elementAt(0)),
      ),
      drawer: AppDrawer(
        rssFeedsMap: rssFeedsMap,
        selectedFeed: selectedFeed,
        changeFeed: changeFeed,
      ),
      body: Column(
        children: [
          Expanded(
            child: ItemsScreen(
              url: rssFeedsMap.elementAt(selectedFeed).values.elementAt(1),
            ),
          ),
        ],
      ),
    );
  }
}
