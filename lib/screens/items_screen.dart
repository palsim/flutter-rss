import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/rss_feed.dart';
import '../widgets/item.dart';

class ItemsScreen extends StatelessWidget {
  final String url;

  const ItemsScreen({required this.url, Key? key}) : super(key: key);

  Future<void> _refreshRSS(BuildContext context) async {
    await Provider.of<RSSFeed>(context, listen: false).fetchAndSetRSS(url);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: _refreshRSS(context),
        builder: (ctx, snapshot) =>
            snapshot.connectionState == ConnectionState.waiting
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : RefreshIndicator(
                    onRefresh: () => _refreshRSS(context),
                    child: Consumer<RSSFeed>(
                      builder: (ctx, rssData, _) => Padding(
                        padding: const EdgeInsets.all(0),
                        child: ListView.builder(
                            itemCount: rssData.items.length,
                            itemBuilder: (_, i) => Column(
                                  children: [
                                    Item(
                                      id: rssData.items[i].id,
                                      title: rssData.items[i].title,
                                      pubDate: rssData.items[i].pubDate,
                                      feed: Provider.of<RSSFeed>(context,
                                              listen: false)
                                          .feed,
                                    ),
                                    const Divider(),
                                  ],
                                )),
                      ),
                    ),
                  ),
      ),
    );
  }
}
