import 'package:flutter/material.dart';
import 'package:html/parser.dart' show parse;
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import '../helpers/image_helper.dart';
import '../helpers/url_helper.dart';
import '../providers/rss_feed.dart';
import '../providers/rss_item.dart';

class ItemScreen extends StatefulWidget {
  static const routeName = "/item";

  const ItemScreen({Key? key}) : super(key: key);

  @override
  State<ItemScreen> createState() => _ItemScreenState();
}

class _ItemScreenState extends State<ItemScreen> {
  RSSItem? _item;
  var _isInit = true;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      final id = ModalRoute.of(context)!.settings.arguments;
      if (id != null) {
        _item =
            Provider.of<RSSFeed>(context, listen: false).findById(id as String);
      }
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          _item!.title,
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.link),
            onPressed: () {
              UrlHelper.launchURL(_item!.link, true);
            },
          ),
        ],
      ),
      body: _item == null
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      _item!.title,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 20,
                        height: 1.5,
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Text(
                      DateFormat.yMMMd().add_jm().format(_item!.pubDate),
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    if (_item!.enclosure != null &&
                        ImageHelper.isImage(_item!.enclosure!.type))
                      Image.network(_item!.enclosure!.url as String),
                    if (_item!.enclosure != null &&
                        ImageHelper.isImage(_item!.enclosure!.type))
                      Text(
                        _item!.enclosure!.type as String,
                        style: const TextStyle(
                          fontSize: 8,
                        ),
                      ),
                    if (_item!.enclosure != null &&
                        ImageHelper.isImage(_item!.enclosure!.type))
                      const SizedBox(
                        height: 16,
                      ),
                    Text(
                      parse(_item!.description).body!.text,
                      textAlign: TextAlign.justify,
                      style: const TextStyle(
                        fontSize: 18,
                        height: 1.5,
                        // letterSpacing: 1.0,
                      ),
                    ),
                    // IconButton(
                    //   icon: const Icon(Icons.link),
                    //   onPressed: () {
                    //     UrlHelper.launchURL(_item!.link);
                    //   },
                    // ),
                    const SizedBox(
                      height: 16,
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
