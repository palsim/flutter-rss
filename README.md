# flutter_rss

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

# Notes

- flutter.yaml error

  > flutter pub add --dev flutter_lints

- [build apk](https://flutter.dev/docs/deployment/android#build-an-apk)

  > flutter build apk --split-per-abi

- [icons tool](http://www.applicationloader.net/appuploader/onlinetools.php)

- [splash tool](https://apetools.webprofusion.com/#/tools/imagegorilla)

- [splash - use this](https://www.youtube.com/watch?v=8ME8Czqc-Oc)

  > flutter clean\
  > flutter pub get\
  > flutter pub run flutter_native_splash:create

- [url launcher](https://pub.dev/packages/url_launcher)

- [app launcher](https://pub.dev/packages/flutter_appavailability)
